/*!
gestion d'une frise specialiser pour afficher les eres geologiques
l'idHTML du support doit etre creer et la page chaerger avant de creer l'instance frise
*/

friseChapitre=null;
function createFriseChapitre()
{
friseChapitre=new Tfrise({
	'idHTML':'friseChapitre_support'
	,'supportRegles':'friseChapitre_supportRegles'
	,'supportDatas':'friseChapitre_supportDatas'
	,'bandeModele':{'date_deb': {"an": 2014,"ms":9,"jr":15,'unite':'jr'},"date_fin": {"an": 2015,"ms":2,"jr":10,'unite':'jr'},'left_marge': 0,'longueur': 1000,'top': 0,'hauteur': 10}
	});

friseChapitre.styleInit=function()
	{
	// -- initialisation des couleurs -- //
	this.evtStyle=new Array();
	this.evtStyle.backgroundColor=new Array();
	// == superEons == //
	// == Eons == //
	this.evtStyle.backgroundColor['Hadeen']='#702';
	this.evtStyle.backgroundColor['Archeen']='#F22';
	this.evtStyle.backgroundColor['Proterozoique']='#F55';
	this.evtStyle.backgroundColor['Phanerozoique']='#F99';
	}


friseChapitre.auto=function()
	{
	this.creer_regleA();
//	this.creerReperesAuto({'callbacks':{'texte':'Million','title':'details'}});
	this.creerReperesAuto({"repereNb":5});

	this.creer_bandeChapitres();
	this.creer_bandeScenarios();
	this.creer_bandeSemaines();

	this.showEvt_Chapitres();
	this.showEvt_Scenarios();
	this.showEvt_Semaines();

//	this.creer_bandeSuperEons();
//	this.showEvt_Niveau('superEons',this.gestBandes.bandesData['superEons']);

	}

friseChapitre.creer_regleA=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "regle",idHTML_parent:this.supportRegles, "idHTML": "regleA","nom":"regleA","top":0,"hauteur":30,"title": ""},options);
	}

friseChapitre.creer_bandeChapitres=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeChapitres","nom":"Chapitres","top": 0,"hauteur":10,"title": "duree des chapitres"},options);
	}

friseChapitre.creer_bandeScenarios=function()
	{
	var options={"opacity":0.75};
	this.gestBandes.creerBande({'genre': 'data',idHTML_parent:this.supportDatas,'idHTML': 'bandeScenarios','nom':'Scenarios',"top": 10,'hauteur':10,"title": "période inter scénario"},options);
	}

friseChapitre.creer_bandeSemaines=function()
	{
	var options=undefined;
	this.gestBandes.creerBande({"genre": "data",idHTML_parent:this.supportDatas,"idHTML": "bandeSemaines","nom":"Semaines","top": 20,"hauteur":10,"title": "duree des semaines"},options);
	}



	// == function d'ajout des evt ==//
friseChapitre.showEvt_Niveau=function(niveau,bandeRef)
	{
	showActivity("ajoute un groupe d'evt",1);
	var t=eresGeologique[niveau];
	if(!t)return;
	if(typeof(bandeRef)!=='object'){showActivity("bandeRef n'est pas un objet!",1);return;}

	var global_Tfrise_msg='ajout de des eres dans la bande '+bandeRef.idHTML;
	for(var p in t)
		{
		if(typeof(t[p])=='object')
			{
			var out=t;
			out=p;
			var EvtNu=bandeRef.evtAdd(
				t[p]
				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}
				,{
					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
			// == application du style personnel a l'evt == //
			var attr={};
			attr.texte=p;
			if(this.evtStyle.backgroundColor[p])attr.backgroundColor=this.evtStyle.backgroundColor[p];
			attr.click=function()
			 	{
			 	alert('click sur evt:'+this.innerHTML,1);
			 	}
			attr.dblclick=function()
		 		{
		 		alert('DOUBLE click sur repere:'+this.innerHTML,1);
		 		}
			this.gestBandes.bandesData[bandeRef.nom].evt[EvtNu].setAttrCSS(attr);
			}
		}
	}

friseChapitre.showEvt_Chapitres=function()
	{
	var bandeRef=friseChapitre.gestBandes.bandesData.Chapitres;
	var EvtNu=bandeRef.evtAdd(
				dijonPraxisChapitre[1][0]
/*				,{'classe': 'evt_'+niveau,'opacity': 1,'zIndex': 1005}*/
				,{
//					 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//					,fin: function(t){showActivity(global_Tfrise_msg,'add')}
				 }
				);
	}

friseChapitre.showEvt_Scenarios=function()
	{
	var bandeRef=friseChapitre.gestBandes.bandesData.Scenarios;
	for (var scNu=1;scNu<6;scNu++){
		var EvtNu=bandeRef.evtAdd(
			dijonPraxisChapitre[1][scNu][0]
			,{"classe": "evt_scenario", "opacity": 1,"zIndex": 1005}
			,{
//				 deb: function(t){showActivity(global_Tfrise_msg,'add')}
//				,fin: function(t){showActivity(global_Tfrise_msg,'add')}
			 }
			);
	}
	}
friseChapitre.showEvt_Semaines=function()
     {
     var bandeRef=friseChapitre.gestBandes.bandesData.Semaines;
     var scNu=1,smNu=1;
     var c=dijonPraxisChapitre[1];
     for (var scNu=1;scNu<6;scNu++)
     for (var smNu=1;smNu<5;smNu++){
	  if(typeof(c[scNu][smNu])!=='object')continue;
	  var EvtNu=bandeRef.evtAdd(
	       c[scNu][smNu]
	       ,{"classe": "evt_semaine","opacity": 1,"zIndex": 1005}
	       ,{
//	            deb: function(t){showActivity(global_Tfrise_msg,'add')}
//		    ,fin: function(t){showActivity(global_Tfrise_msg,'add')}
	       }
	       );
	  }
     }

//initialisation
//friseChapitre.styleInit();

}	//createFriseChapitre
