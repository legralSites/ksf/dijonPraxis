#!/bin/sh

echo "telechargement avec rendu statique d'un site";

#dirname="${1%${1##*/}}"
dirname=`dirname $0`
url="http://127.0.0.1/git/vampire/index.php";
echo "dirname: ${dirname}";

source ./$dirname/couleur.sh



# - on se positionne dans le repertoire du script - #
cd  ${dirname} 

# - on remonte a la racine du projet - #
cd ..

echo "$couleurINFO repertoire de travail$couleurNORMAL";
pwd

echo "$couleurINFO suppression de ../statique/$couleurNORMAL";
rm -R ./statique/;
echo "$couleurINFO creation de ../statique/$couleurNORMAL";
mkdir ./statique/;
echo "$couleurINFO entree dans ../statique/$couleurNORMAL";
cd   ./statique/;

echo "$couleurINFO téléchargement...$couleurWARN";
#--no-verbose --quiet
wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url

echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
mv ./git/vampire/ ./

# - activer la suppression apres verifiacation - #
rm -R ./git/

# - (push) online - #
if [ "$1" == "online" ];then
        echo "$couleurINFO upload sur legral$couleurNORMAL";
        # http://lftp.yar.ru/
        #lftp ftp://legral@ftp.legral.fr -e "mirror -e -R -x dossier_ignoré -x dossier_ignoré /emplacement_local /emplacement_distant ; quit"
        lftp ftp://legral@ftp.legral.fr -e "mirror -e -R ./statique/ /intersites/lib/perso/js/repereTemporel/ ; quit"
        fi


