#!/bin/sh
#### colorisation #############################################
BOLD=`tput smso`
NOBOLD=`tput rmso`

#NORMAL="\[\e[0m\]"
BLACK=`tput setaf 0`
#RED="\[\e[1;31m\]"
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

couleurNORMAL=$WHITE
couleurINFO=$BLUE
couleurCOM=$YELLOW
couleurWARN=$RED
couleurTITRE1=$GREEN
couleurTITRE2=$MAGENTA

export BOLD
export NOBOLD
export BLACK
export RED
export GREEN
export YELLOW
export CYAN
export MAGENTA
export BLUE
export WHITE

export couleurNORMAL
export couleurINFO
export couleurCOM
export couleurWARN
export couleurTITRE1
export couleurTITRE2
