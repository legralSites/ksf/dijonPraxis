#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier

# - configuration - #
#noStatus =1;	# ne aps affichier le status -> pas pris en compte ->kle changer dans ./scritps/gitVersion.sh
#export noStatus

function localConcat {
        # - concatenation des fichiers css - #
	echo "$couleurINFO # - concatenation des fichiers css - #$couleurNORMAL";
        catCSS ./styles/knacss.css
        catCSS ./styles/gestLib-0.1.css

	catCSS ./styles/html4.css
	catCSS ./styles/intersites.css
	catCSS ./styles/dijonPraxis.css
	#catCSS ./cgp/frises-chapitres.css

	catCSS ./styles/notes/notesLocales.css
	catCSS ./styles/menuOnglets-defaut/menuLocales.css
	catCSS ./styles/tutoriels/tutorielsLocales.css


        # - concatenation des fichiers js - #
	echo "$couleurINFO # - concatenation des fichiers js - #$couleurNORMAL";
        #catJS /www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js
	catJS ./lib/legral/php/gestLib/gestLib-v1.0.0.js

	#catJS ./lib/legral/js/timeline.min-v1.2.2.js
	catJS ./lib/legral/js/timeline-v1.2.2.js


	# - cp de pages - #
	echo "$couleurINFO # - copie de pages - #$couleurNORMAL";
	cp -R /www/0pages/_site/ ./pagesLocales/_site/

	# - statification - #
	# -- personaliser -- #
#	./scripts/statique.sh
	}

function localSave {
	# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
	versionSave ./scripts/gitVersion sh
	versionSave ./localVersion sh



	# - statification - #
	# -- personaliser et decommenter -- #
	echo "$couleurINFO # - statification - #$couleurNORMAL";
	if [ -f './scripts/statique.sh' ];then
                echo "$couleurINFO # - on statifie - #$couleurNORMAL";
               ./scripts/statique.sh
        else
                echo "$couleurWARN pas de script de statification$couleurNORMAL";
               fi

#	echo "$couleurINFO # - mise en ligne de la version statique - #$couleurNORMAL";
	#lftp ftp://legral@ftp.legral.fr -e "mirror -e -R -x dossier_ignoré -x dossier_ignoré /emplacement_local /emplacement_distant ; quit"
#	lftp ftp://legral@ftp.legral.fr -e "mirror -e -R  ./statique/vampire /www/dijonPraxis ; quit"

	}


