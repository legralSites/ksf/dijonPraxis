<?php
$ext='.html';
// ==== menu: primaire ==== //
$mn='dijonPraxis';
$rl=PAGES_ROOT.$mn.'/';	// Repertoire Locale du projet dans les pages communes
$pn='accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);                           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$pn);               // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',"$mn - $pn");       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',"$mn - $pn");   // titre de la page: afficher dans le bas de page
//      $m->setMeta($pn,'title','tutoriels - accueil(meta)');   // meta <title> (si non definit title=titre)
//      $m->addCssA($pn,'dossier1');                          // applique le style dossier1 a la balise <a>


$pn='regles';
$m->addCallPage("$mn-$pn",'');        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr("$mn-$pn",'menuTitre','règles');
        $m->setAttr("$mn-$pn",'titre',"$mn - $pn");
        $m->addCssA("$mn-$pn",'dossier1');            // applique le style dossier1 a la balise <a>


$pn='obediences';
$m->addCallPage("$mn-$pn",'');        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr("$mn-$pn",'menuTitre',$pn);
        $m->setAttr("$mn-$pn",'titre',"$mn - $pn");
        $m->addCssA("$mn-$pn",'dossier1');            // applique le style dossier1 a la balise <a>


$pn='cartoGralPhie';
$m->addCallPage("$mn-$pn",'');        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr("$mn-$pn",'menuTitre',$pn);
        $m->setAttr("$mn-$pn",'titre',"$mn - $pn");
        $m->addCssA("$mn-$pn",'dossier1');            // applique le style dossier1 a la balise <a>

$pn='scenaristiques';
$m->addCallPage("$mn-$pn",'');        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr("$mn-$pn",'menuTitre','scénaristiques');
        $m->setAttr("$mn-$pn",'titre',"$mn - $pn");
        $m->addCssA("$mn-$pn",'dossier1');            // applique le style dossier1 a la balise <a>


$pn='lexiques';
$m->addCallPage($pn,"$rl$pn$ext");        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');            // applique le style dossier1 a la balise <a>



// ==== menu: regles ==== //
$mn='dijonPraxis-regles';
$pn='accueil';
$m=$gestMenus->addMenu($mn,$pn,$rl.'regles-'.$pn.$ext);
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);                           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$pn);               // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',"$mn - $pn");       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',"$mn - $pn");   // titre de la page: afficher dans le bas de page

/*
$m->addCallPage($pn,$rl.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
*/
$pn='persoCreation';
$m->addCallPage($pn,$rl.'regles-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");

$pn='combats';
$m->addCallPage($pn,$rl.'regles-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");

$pn='influences';
$m->addCallPage($pn,$rl.'regles-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
	$m->addCssA($pn,'dossier1');


// ==== menu: obediences ==== //
$mn='dijonPraxis-obediences';
$pn='dijonPraxis-obediences-accueil';
$m=$gestMenus->addMenu($mn,$pn,$rl.'obedience-accueil'.$ext);
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);                   // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre','obédiences');               // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
//        $m->setAttr($pn,'menuTitle',$pn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//	$m->addCssA($pn,'dossier1');


$pn='obedience-Camarillia';
$m->addCallPage($pn,$rl.$pn.'-accueil'.$ext);        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr($pn,'menuTitre','Camarillia');
        $m->setAttr($pn,'titre',"$mn - $pn");
        $m->addCssA($pn,'dossier1');            // applique le style dossier1 a la balise <a>

$pn='obedience-Sabbat';
$m->addCallPage($pn,$rl.$pn.'-accueil'.$ext);        // si un sous menu a le meme nom (index) q'une page alrs ce  sous menu sera appelle. Pas besoin de preciser une page a appelle
        $m->setAttr($pn,'menuTitre','Sabbat');
        $m->setAttr($pn,'titre',"$mn - $pn");
        $m->addCssA($pn,'dossier1');            // applique le style dossier1 a la balise <a>

$pn='obedience-Anarch';
$m->addCallPage($pn,$rl.$pn.'-accueil'.$ext);
        $m->setAttr($pn,'menuTitre','Anarch');
        $m->setAttr($pn,'titre',"$mn - $pn");
	$m->addCssA($pn,'dossier1');



// ==== menu: camarilla factions ==== //
$mn='obedience-Camarillia';
$pn=$mn.'-accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($pn,'dossier1');

$pn='Gangrel';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Brujah';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//      $m->addCssA($pn,'dossier1');


$pn='Malkavien';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Toreador';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Ventrue';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Nosferatu';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');


$pn='Tremere';
$m->addCallPage($pn,"$rl".'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');


// ==== menu: Sabbat factions ==== //
$mn='obedience-Sabbat';
$pn=$mn.'-accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($pn,'dossier1');

$pn='Lasombra';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Tzimisce';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

// ==== menu: Anarch factions ==== //
$mn='obedience-Anarch';
$pn=$mn.'-accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($pn,'dossier1');

$pn='Assamite';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Giovanni';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$pn='Ravnos';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');


$pn='Sethite';
$m->addCallPage($pn,$rl.'faction-'.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');


// ==== menu: Anarch factions ==== //
$rl0=$rl;	// sauvegarde du repertoire local
$rl.='cgp/';
$mn='dijonPraxis-cartoGralPhie';
$pn=$mn.'-accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'visible',1);           // 0: le li ne sera pas affiche 1:afficher
        $m->setAttr($pn,'menuTitre',$mn);       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($pn,'dossier1');

$pn='contexte';
$m->addCallPage($pn,$rl.$pn.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

$rl=$rl0;	// restauration du repertoire local
unset ($rl0);

// ==== menu: scenaristiques ==== //
$mn='dijonPraxis-scenaristiques';
$pn=$mn.'scenaristiques-accueil';
$m=$gestMenus->addMenu($mn,$pn,"$rl$pn$ext");
        // -- parametrer la page -- //
        $m->setAttr($pn,'menuTitre','scénaristiques');       // afficher dans l'onglet (s'il correspond a un memnu , celui ci sera appelle) 
        $m->setAttr($pn,'menuTitle',$mn);       // afficher au survol du titre (ariane et onglet) si pas defini alors menuTitle=menuTitre
        $m->setAttr($pn,'titre',$pn);   // titre de la page: afficher dans le bas de page
//        $m->addCssA($pn,'dossier1');

$pn='chapitre1';
$m->addCallPage($pn,$rl.'scenes-chapitre1'.$ext);
        $m->setAttr($pn,'menuTitre',$pn);
        $m->setAttr($pn,'titre',"$mn - $pn");
//        $m->addCssA($pn,'dossier1');

?>



